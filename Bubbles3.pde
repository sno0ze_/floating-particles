Bubble[] bb = new Bubble[5000];


void setup() {
  fullScreen(P2D);
  //size(1920, 1080);
  
  for(int i = 0; i < bb.length; i++) {
    bb[i] = new Bubble();
  }
}

void draw() {
  background(45, 75, 45);
  for(int i = 0; i < bb.length; i++) {
    bb[i].display();
    bb[i].ascend();
    bb[i].top();
  } 
}  
